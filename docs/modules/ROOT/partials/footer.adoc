
== COPYRIGHT

{copyright} {author} {year}

The NXLog Community Edition is licensed under the NXLog Public License. The
NXLog Enterprise Edition is not free and has a commercial license.
