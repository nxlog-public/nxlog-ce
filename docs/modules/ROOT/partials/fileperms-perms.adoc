[id='{module}_config_perms']
Perms:: This directive specifies the permissions to use for the created
  socket or pipe or file. This must be a four-digit octal value beginning with a zero. By
  default, OS default permissions will be set.
  This directive is not currently supported on Windows.

