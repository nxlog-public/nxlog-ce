:description: This topic elaborates on how to process and forward log data with methods written in the Python language.
:keywords: log data,python script,log level,debug log,event record,python method,python code,nxlog
:module: om_python
[[om_python]]
[desc="Provides a Python API for saving or forwarding log data"]
==== Python (om_python)

This module provides support for forwarding log data with methods written in
the link:https://www.python.org[Python] language. Currently,
only versions 3.x of Python are supported.

The file specified by the <<om_python_config_pythoncode,PythonCode>> directive
should contain a `write_data()` method which is called by the _om_python_
module instance. See also the <<xm_python,xm_python>> and
<<im_python,im_python>> modules.

include::../see_modules_by_pkg.adoc[]

The Python script should import the `nxlog` module, and will have access to
the following classes and functions.

include::../extension/python.adoc[tag=log_functions]

_class_ nxlog.Module:: This class is instantiated by {productName} and can
  be accessed via the <<om_python_api_logdata_module,LogData.module>>
  attribute. This can be used to set or access variables associated with the
  module (see the <<om_python_config_examples,example>> below).

[[om_python_api_logdata]]
_class_ nxlog.LogData:: This class represents an event. It is instantiated
  by {productName} and passed to the `write_data()` method.
+
--
delete_field(name)::: This method removes the field _name_ from the event
  record.
field_names()::: This method returns a list with the names of all the fields
  currently in the event record.
get_field(name)::: This method returns the value of the field _name_ in the
  event.
set_field(name, value)::: This method sets the value of field _name_ to
  _value_.

[[om_python_api_logdata_module]]
module::: This attribute is set to the Module object associated with the
  LogData event.
--

[[om_python_config]]
===== Configuration

The _om_python_ module accepts the following directives in addition to the
<<config_module_common,common module directives>>.

[[om_python_config_pythoncode]]
PythonCode:: This mandatory directive specifies a file containing Python
  code. The _om_python_ instance will call a `write_data()` function which must
  accept an <<om_python_api_logdata,nxlog.LogData>> object as its only
  argument.

[[om_python_config_call]]
Call:: This optional directive specifies the Python method to invoke. With
  this directive, you can call only specific methods from your Python code.
  If the directive is not specified, the default method `write_data` is invoked.

[[om_python_config_examples]]
===== Examples

[[om_python_example1]]
.Forwarding events with om_python
====
This example shows an alerter implemented as an output module instance in
Python. First, any event with a normalized severity less than of 4/ERROR is
dropped; see the <<config_module_exec,Exec>> directive (_xm_syslog_ and most
other modules set a normalized
<<xm_syslog_field_SeverityValue,$SeverityValue>> field). Then the Python
function generates a custom email and sends it via SMTP.

.nxlog.conf
[source,config]
----
include::example$om_python.conf[lines=6..-1]
----

.output.py
[source,python]
----
include::example$om_python_output.py[]
----
====
