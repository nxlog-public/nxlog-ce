#ifndef __NX_INET_PTON_H
#define __NX_INET_PTON_H

int nx_inet_pton(int af, const char *src, void *dst);

#endif	/* __NX_INET_PTON_H */